
## Links go to visit what docker is running locally.
prometheus:     http://localhost:9090
grafana:        http://localhost:3000
registry:       http://localhost:5000/v2/_catalog
onedev:         http://localhost:6610

## How to use locally run registry to upload a docker image manually.
### Step 1 choice an image you want to upload.
Run this to check for image you have.
```
docker image ls
```
Remeber the name and tag of the one you want to use.

If you dont have one you want you will have to create it or pull it down first.
Will for as an example for all steps use alpine and we will need to pull it first with.
```
docker image pull alpine
```

### step 2 create docker image tag
Create a new tag which repository point to locally run registry by running.
```
docker tag alpine:latest localhost:5000/alpine:latest
```
Change alpine and latest to controll which docker image you want to use. Change 5000 to what port your locally registry is using.

Use

```
docker image ls
```
To verify that everything went well.

### step 3 push docker image to local registry

Next is to push the docker image to your local registry by using.
```
docker push localhost:5000/alpine:latest
```
Change alpine and latest to controll which docker image you want to use. Change 5000 to what port your local registry is using.

Visit http://localhost:5000/v2/_catalog (change port if needed) to verify that the docker image exist on your local registry
