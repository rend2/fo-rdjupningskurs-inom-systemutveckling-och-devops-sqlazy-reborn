
## Git branching strategi.
Vi tänker att vi i början jobbar tillsammans direkt på main branchen för att sätta upp en bra grund med docker och kanske ju mer vi upptäcker på vad vi kan/vill göra tillsammans.

Sen kommer vi börja använda branches för olika delar av projektet för att tydligt separera olika saker som behöves göras, som senare när det är klart eller en tydlig fungerande bit fungerar så kommer vi merga in det i main brachen.

## Git branching strategy
We plan to start out working on the main branch together until we have the foundation, e.g. docker, compose file. 

Later we'll start using branches to develop separate "features", that when functioning can be merged into the main branch.